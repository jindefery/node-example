# Bookinfo Rating Service

Rating service has been developed on NodeJS

## License

MIT License

## How to run with Docker

```bash
docker-compose up
```

* Test with path `/ratings/1`

